package com.example.drawer;

//import com.ctc.android.widget.R;
import com.example.drawer.ImageMap;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

	ImageMap mImageMap;
	boolean isStart=true;
	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
		
		
	}
	


	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager
				.beginTransaction()
				.replace(R.id.container,
						PlaceholderFragment.newInstance((position + 1),isStart)).commit();
		if(isStart==true)
			isStart=false;
		
	}
	//untuk menampilkan fitur slash
	public void onSectionAttached(int number) {
		switch (number) {
		case 1:
			mTitle = getString(R.string.title_section1);
			break;
		case 2:
			mTitle = getString(R.string.title_section2);
			break;
		case 3:
			mTitle = getString(R.string.title_section3);
			break;
		case 4:
			mTitle = getString(R.string.title_section4);
			break;
		case 5:
			mTitle = getString(R.string.title_section5);
			break;
		case 6:
			mTitle = getString(R.string.title_section6);
			break;
		case 7:
			mTitle = getString(R.string.title_section7);
			break;
		case 8:
			mTitle = getString(R.string.title_section8);
			break;
		case 9:
			mTitle = getString(R.string.title_section9);
			break;
		case 10:
			mTitle = getString(R.string.title_section10);
			break;
		case 11:
			mTitle = getString(R.string.title_section11);
			break;
		case 12:
			mTitle = getString(R.string.title_section12);
			break;
		case 13:
			mTitle = getString(R.string.title_section13);
			break;
		case 14:
			mTitle = getString(R.string.title_section14);
			break;
		case 15:
			mTitle = getString(R.string.title_section15);
			break;
		case 16:
			mTitle = getString(R.string.title_section16);
			break;
		case 17:
			mTitle = getString(R.string.title_section17);
			break;
		case 18:
			mTitle = getString(R.string.title_section18);
			break;
		case 19:
			mTitle = getString(R.string.title_section19);
			break;
		case 20:
			mTitle = getString(R.string.title_section20);
			break;
		case 21:
			mTitle = getString(R.string.title_section21);
			break;
		case 22:
			mTitle = getString(R.string.title_section22);
			break;
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		
		actionBar.setDisplayShowTitleEnabled(true);
		if(isStart==false)
			mTitle=getString(R.string.app_name);
			
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
//		if (!mNavigationDrawerFragment.isDrawerOpen()) {
//			// Only show items in the action bar relevant to this screen
//			// if the drawer is not showing. Otherwise, let the drawer
//			// decide what to show in the action bar.
//			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
//			return true;
//		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		ImageMap mImageMap;
		TextView infoarea;
		public int sectionn=0;
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber,boolean isStart) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			if(isStart==false)
			fragment.sectionn=sectionNumber;
			
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			int resid=0;
			//memanggil gambar yg udah ada garis arahnya
			switch(sectionn){
			
			case 1:resid=R.drawable.umya;break;
			case 2:resid=R.drawable.umyb;break;
			case 3:resid=R.drawable.umyc;break;
			case 4:resid=R.drawable.umyd;break;
			case 5:resid=R.drawable.umye1;break;
			case 6:resid=R.drawable.umye2;break;
			case 7:resid=R.drawable.umye3;break;
			case 8:resid=R.drawable.umye4;break;
			case 9:resid=R.drawable.umye5;break;
			case 10:resid=R.drawable.umyf1;break;
			case 11:resid=R.drawable.umyf2;break;
			case 12:resid=R.drawable.umyf3;break;
			case 13:resid=R.drawable.umyf4;break;
			case 14:resid=R.drawable.umyf5;break;
			case 15:resid=R.drawable.umyf6;break;
			case 16:resid=R.drawable.umyf7;break;
			case 17:resid=R.drawable.umyg1;break;
			case 18:resid=R.drawable.umyg2;break;
			case 19:resid=R.drawable.umyg3;break;
			case 20:resid=R.drawable.umyg4;break;
			case 21:resid=R.drawable.umyg5;break;
			case 22:resid=R.drawable.umyg6;break;
			default:resid=R.drawable.umy;break;
			}
//			resid=R.drawable.umy;
			// find the image map in the view
	        mImageMap = (ImageMap)rootView.findViewById(R.id.map);
		    mImageMap.setImageResource(resid);
		    infoarea = (TextView)rootView.findViewById(R.id.infoarea);
		    infoarea.setText("Deskripsi:\nSentuh gedung untuk mendapatkan deskripsi, pinch layar untuk melakukan zoom pada peta");
	        
	        // add a click handler to react when areas are tapped
		    //mnampilkan deskripsi
	        mImageMap.addOnImageMapClickedHandler(new ImageMap.OnImageMapClickedHandler()
	        {
				@Override
				public void onImageMapClicked(int id, ImageMap imageMap)
				{
					// when the area is tapped, show the name in a 
					// text bubble
					mImageMap.showBubble(id);
					Log.d("IDMAP", String.valueOf(id));
					try{
						String desc=mImageMap.getAreaAttribute(id, "desc");
						infoarea.setText("Deskripsi:\n"+desc);
					}catch(NullPointerException ne){
						infoarea.setText("Deskripsi:\n");
					}catch(Exception e){
						infoarea.setText("Deskripsi:\n");
					}
				}

				@Override
				public void onBubbleClicked(int id)
				{
					// react to info bubble for area being tapped
				}
			});
			return rootView;
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			((MainActivity) activity).onSectionAttached(getArguments().getInt(
					ARG_SECTION_NUMBER));
		}
	}

}
